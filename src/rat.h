#ifndef RATIONAL_RAT_H
#define RATIONAL_RAT_H

#include <iostream>

class Rat
{
public:
    Rat(int num, int den=1);
    Rat(double value, int accuracy);

    int getNumerator() const;
    int getDenominator() const;

    Rat operator+() const;    // +Rat
    Rat operator-() const;    // -Rat

    Rat operator+(const Rat operand) const;  // Rat + Rat
    Rat operator-(const Rat operand) const;  // Rat - Rat
    Rat operator*(const Rat operand) const;  // Rat * Rat
    Rat operator/(const Rat operand) const;  // Rat / Rat

    Rat operator+=(Rat operand);   // Rat+=Rat
    Rat operator-=(Rat operand);   // Rat-=Rat

    Rat operator++();              // ++Rat  prefix
    Rat operator++(int r);         // Rat++  postfix
    Rat operator--();              // --Rat
    Rat operator--(int r);         // Rat--

    bool operator==(Rat r);        // Rat == Rat
    bool operator!=(Rat r);        // Rat != Rat
    bool operator<(const Rat r) const ;         // Rat < Rat
    bool operator>(const Rat r) const ;         // Rat > Rat
    bool operator<=(const Rat r) const;        // Rat <= Rat
    bool operator>=(const Rat r) const;        // Rat >= Rat

    float toFloat() const;
    friend std::ostream& operator<<(std::ostream& s, Rat r);

private:
};
#endif //RATIONAL_RAT_H
